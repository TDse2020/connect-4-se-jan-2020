# GitLab
- 

# Site
- Randy's Demo For Starter Code


1. 7x6 grid on the page.
    -Use for loops to make grid
2. Player can add discs to the board.
    -Create Player 1 as red
    -Create Player 2 as black
    -Keep track of Player moves
3. Disc color alternates between players.
      -Change color of circle when player clicks on column (click handler)
      -How will you make the disks stack?
4. Game checks for wins vertically, horizontaly, and diagonally, and lets user know when the game ends in a Red Win, a Black Win, or a Tie.



# Assessment: Connect-Four
In Connect-Four, one player is assigned Red and the other player is assigned Black. Players alternate inserting their pieces into one of the 7 columns of a 7x6 grid. The first player to get four of their pieces in a row (horizontal, vertical, or diagonal), wins.

In the physical world, the grid is placed perpendicular to the playing table, such that the pieces fall to the bottom and stack upon each other. Your digital version of the game should yield the same end result (the falling animation is optional).

The game can end in a tie, meaning all cells are filled but no player achieved four-in-a-row.

Implement Connect-Four using HTML, CSS, and JavaScript. In each game, the players will be sitting at the same mouse & keyboard, alternating turns.

Assessment Details
Although we encourage you to collaborate with other students for ideas or debugging, you will code and create your own game.

This will be a project you include in your portfolio, so make it look nice, and use GitLab's Pages feature to host your finished game so that anyone can play it.

# Hints
If you are unsure how to get started, here are some incremental milestones:

- Display a red or black disc.
- Stack red and black discs in a column using a flex box layout.
- Display a full board consisting of 7 columns.
- Set a click handler function for each column that adds an additional disc.
- Take turns! Toggle the color of each successive disc added.
- Keep track of what color disc is at each position in the board.     
- You should be able to console.log() debugging output after each move showing the state of the board.
- Once a column is full (has 6 discs), don't allow any more discs to be added.
- Check whether the last disc added completed a four-in-a-row within the column (vertically).
- Check whether the last disc added completed four-in-a-row horizontally.
- Check whether the last disc added completed four-in-a-row on either an upward- or downward-sloping diagonal.

# Submission

Push your code into your GitLab repository and deploy it via GitLab pages.
Once you have the GitLab Pages url (ex: https://username.gitlab.io/connectFour/), go back to your code to create a README.md file, and put the GitLab Pages url and your thought processes/development plans as the contents.
Push your code to your GitLab repo once more.
In Canvas, please submit your Gitlab Repo url.
In GitLab, add KA_Grading as a member on your project with "Reporter" permission.








